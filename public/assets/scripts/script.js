$(document).ready(function(){
	// za oretragu se koristi textarea ako se trazi po broju
	$('#type').on('change', function() {
		if ($('#type').val() ===  'tm_number') {
			$('#term').replaceWith("<textarea class='form-control' rows='3' name='term' id='term' placeholder='Pretraga'></textarea>");	
		} else {
			$('#term').replaceWith("<input type='text' placeholder='Pretraga' class='form-control' name='term' id='term'>");
		}
	});

    //$('#datatable').DataTable();
});
