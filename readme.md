# Parser application

This application is using custom built console commands that works in specific order:
* checks DB if there is some comparison in que or in progress
* launches new comparison
* pulls data from one api, stores in db and process
* pulls data from second api, stores in db and process
* pulls data from third api, stores in db and process
* do the comparison of the fetched data and generate report that's also saved in db for later usage
* send email notification to the person who requested that report with a link to view results and/or download it in excel

A side from that, application utilizes some custom built helpers for all of this logic, and one specific library for stric string comparison that has heavy CPU usage if you're matching a lot of data.

Application is built with Laravel 5.2.x