@extends('layouts.dashboard')
@section('page_heading','Izmena korisnika')
@section('section')

        <!-- /.row -->
<div class="col-sm-12">
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-8">

        <div class="panel panel-default">
                <div class="panel-heading">Izmena korisnika</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ action('AdminController@postEditUser', ['id' => $user->id]) }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ (isset($user->name)) ? $user->name : ''}}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ (isset($user->last_name)) ? $user->last_name : ''}}">

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ (isset($user->email)) ? $user->email : '' }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('admin') ? ' has-error' : '' }}">
                            <label for="admin" class="col-md-4 control-label">Admin</label>

                            <div class="col-md-6">
                                <select name="admin" id="admin" class="form-control">
                                    <option value="1" {{ ($user->sudo) ? 'selected' : ''}}>Yes</option>
                                    <option value="0" {{ ($user->sudo) ? '' : 'selected'}}>No</option>
                                </select>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Sacuvaj promene
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


           
        </div>
    </div> 
</div>

@stop