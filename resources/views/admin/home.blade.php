@extends('layouts.dashboard')
@section('page_heading','Ohim Dashboard')
@section('section')

        <!-- /.row -->
<div class="col-sm-12">
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $pending }}</div>
                            <div>Na cekanju</div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-inbox fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $finished }}</div>
                            <div>Zavrsenih</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-download fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $working }}</div>
                            <div>U procesu obrade</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-search fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $ukupno_pretraga }}</div>
                            <div>Ukupno pretraga</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-8">

            @section ('pane2_panel_title', 'Responsive Timeline')
            @section ('pane2_panel_body')

                    <!-- /.panel -->



           <table class="table table-hover">
               <thead>
                   <tr>
                       <th>#</th>
                       <th>First Name</th>
                       <th>Last Name</th>
                       <th>Email</th>
                       <th>Option</th>
                   </tr>
               </thead>
               <tbody>
                   @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->last_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <a class="btn btn-outline btn-primary btn-xs" href="{{ action('AdminController@showEditUser', ['id' => $user->id]) }}">Izmeniti</a>
                        </td>
                    </tr>
                   @endforeach
               </tbody>
           </table>

            <!-- /.panel-body -->

            <!-- /.panel -->
            @endsection
            @include('widgets.panel', array('header'=>true, 'as'=>'pane2'))
        </div>
        <!-- /.col-lg-8 -->
        <div class="col-lg-4">

                        <!-- /.panel -->
            @section ('pane3_panel_title', 'Lista modula')
            @section ('pane3_panel_body')

        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Ime</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($modules as $module)
                    <tr>
                    <td>{{ $module->id }}</td>
                    <td>{{ $module->name }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.panel-body -->
        <div class="panel-footer">
            <!--
        <form action="{{ action('AdminController@addModule') }}" method="POST">
        {{ csrf_field() }}
            <div class="input-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <input id="name" type="text" class="form-control input-sm" placeholder="New module" name="name">
                        <span class="input-group-btn">
                            <button class="btn btn-warning btn-sm" id="btn-chat">
                                Dodaj
                            </button>
                        </span>
            </div>
            </form>
            -->
        </div>
        <!-- /.panel-footer -->
    </div>
    <!-- /.panel .chat-panel -->
    @endsection
    @include('widgets.panel', array('header'=>true, 'as'=>'pane3'))
</div>

<!-- /.col-lg-4 -->

@stop