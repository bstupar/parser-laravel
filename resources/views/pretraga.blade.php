@extends('layouts.dashboard')
@section('page_heading', 'Pretraga')
@section('section')
           
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            @if ($errors->has('term'))
                        <div class="alert alert-warning">
                            <strong>{{ $errors->first('term') }}</strong>
                        </div>
                    @endif

			<form action="{{ action("OhimController@pretraga") }}" class="form-inline" method="post" role="form">
                {{ csrf_field() }}
				<div class="form-group">
					<label for="searchType">Pretraga po</label>
					<select class="form-control" name="type" id="type">
                        <option value="name" {{ (old('type') === 'name')? 'selected' : ''}}>Imenu Apkikanta</option>
						<option value="owner_id" {{ (old('type') === 'owner_id')? 'selected' : ''}}>Owner ID-u</option>
                        <option value="tm_number" {{ (old('type') === 'tm_number')? 'selected' : ''}}>Trademark Broju</option>
					</select>
				</div>
				<div class="form-group {{ $errors->has('term') ? ' has-error': '' }}">
					<input type="text" placeholder="term" class="form-control" name="term" id="term" value="{{ old('term')}}" autofocus>

				</div>
                <div class="form-group">
                    <div class="col-md-7 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i> Pretrazi
                        </button>
                    </div>
                </div>
			</form>
            	
        </div>
    </div>
    @if ( session()->has('pretraga') )
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

                <table class="table table-hover">
                    <caption>Slicne pretrage</caption>
                    <thead>
                    <tr>
                        <th>Trazeno</th>
                        <th>Status pretrage</th>
                        <th>Izvestaj</th>
                        <th>Datum zahteva</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( session()->get('pretraga') as $row)
                        <tr>
                            <td>
                                @if ($row->type === 'tm_number')
                                    {{ implode(" ", unserialize($row->term)) }} 
                                @else
                                    {{  $row->term}}
                                @endif
                                        
                            </td>
                            <td>
                                @if ($row->status === 0)
                                    U obradi
                                @else
                                    Zavrseno
                                @endif
                            </td>
                            <td><a href="{{ route('download', [$row->id]) }}">Preuzmi izvestaj</a></td>
                            <td>{{ $row->created_at }}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
        </div>
    </div>
    @endif
</div>
            
@stop