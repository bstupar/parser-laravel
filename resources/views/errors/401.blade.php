@extends('layouts.dashboard')
@section('page_heading','Greska 401')
@section('section')

        
<div class="col-sm-12">
    <div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        <div class="well">
            <h1>Opps  <small>Pristup trazenoj stranici nije dozvoljen</small></h1>
            <a href="{{ url('/')}}">Povratak na pocetnu</a>
        </div>
    </div>
    </div>
</div>
@stop