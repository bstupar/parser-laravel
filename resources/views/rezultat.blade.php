@extends('layouts.dashboard')
@section('page_heading', 'Pretraga')
@section('section')
           
<div class="container-fluid" style="margin-top:20px;">
    <div class="row">
        <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <dl class="dl-horizontal">
                        <dt>Tip pretrage</dt>
                        <dd> 
                            @if ($request->type === 'name') 
                                Imenu
                            @elseif ($request->type === 'owner_id')
                                Owner ID
                            @elseif ($request->type === 'tm_number')
                                Trademark Broju
                            @endif
                        </dd>
                        <dt>Parametar pretrage</dt>
                        <dd> 
                            @if ($request->type === 'tm_number')
                                {{ implode(" ", unserialize($request->term)) }} 
                            @else
                                {{  $request->term}}
                            @endif
                        </dd>
                        <dt>Datum pretrage</dt>
                        <dd> {{ $request->created_at }} </dd>
                    </dl>
            </div>
        </div>
            @if (empty($matches))
        <div class="alert alert-warning">
            <p>Trazeni parametri ne spadaju pod deklaraciju 28.8.</p>
        </div>
        @else
        <a class="btn btn-primary btn-xs" href="{{ route('download', [$request->id]) }}">Download excel report</a>
        <a class="btn btn-primary btn-xs" download="tabela.xls" onclick="return ExcellentExport.excel(this, 'datatable', 'tabela');" href="#">Export to Excel</a><br/><br/>
        <div class="col-md-12">
            <table class="table table-striped" id="datatable">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>EUTM No.</th>
                    <th>Application date / Registration date</th>
                    <th>Trademark name</th>
                    <th>Nice Edition</th>
                    <th>Nice Classification in</th>
                    <th>Eligible for declaration</th>
                    <th>Class</th>
                    <th>Complete/Incomplete</th>
                    <th>Class Text</th>
                    <th>Class Heading Text</th>
                    <th>Compared classes</th>
                    <th>Not covered by the literal meaning</th>
                </tr>
                </thead>
                <tbody>

                <?php
                
                $redni_br = 0;
                foreach ( $matches as $num => $match ) {

                    
                    $redni_br++;
                    // loop po svakoj nice class i printuj ceo row
                    
                    foreach ( $match->niceclasses as $val ) {
                        
                        echo "<tr><td>{$redni_br}</td>";
                        echo "<td><a href='https://euipo.europa.eu/eSearch/#details/trademarks/{$num}'>" . $num . '</a></td>';
                        echo '<td>' . $match->regdate.'</td>';
                        echo '<td>' . $match->trademark . '</td>';
                        echo '<td>' . $match->nice_edition . '</td>';
                        echo '<td>Edition ' . $match->edition . '</td>';
                        // we have full match
                        // if ( isset($match->$val->full_match) ) {
                        //     if ($match->$val->full_match) {
                        //         echo '<td>'. $match->declaration .'</td>';
                        //         echo '<td>'. $val.'</td>';
                        //         echo '<td>'. $match->status .'</td>';
                        //         echo '<td>'. $match->$val->class_description .'</td>';
                        //         echo '<td>'. $match->$val->class_headings .'</td>';
                        //         //echo '<td>'. App\Lib\Helper::namelyBeing($match->$val->matched) .'</td>';
                        //         echo '<td>'. $match->$val->matched .'</td>';
                        //         echo '<td>'. $match->$val->anex .'</td>';
                        //     }
                        //     // incomplete match
                        //     if ( $match->$val->full_match === false) {
                        //         echo '<td>'. $match->declaration .'</td>';
                        //         echo '<td>'. $val.'</td>';
                        //         echo '<td>'. $match->status .'</td>';
                        //         echo '<td>'. $match->$val->class_description .'</td>';
                        //         echo '<td>'. $match->$val->class_headings.'</td>';
                        //         //echo '<td>'. App\Lib\Helper::compareStrColored( $match->$val->matched, $match->$val->class_description ) .'</td>';
                        //         echo '<td>'. $match->$val->matched .'</td>';
                        //         echo '<td>'. $match->$val->anex .'</td>';
                        //     }
                        // } if (!isset( $match->$val->full_match)) {
                        //     // not a match at all !
                        //     echo '<td>'. $match->declaration .'</td>';
                        //     echo '<td>'. $val.'</td>';
                        //     echo '<td>'. $match->status .'</td>';
                        //     echo '<td>'. $match->$val->class_description  .'</td>';
                        //     echo '<td>'. $match->$val->class_headings .'</td>';
                        //     echo '<td>'. $match->$val->matched .'</td>';
                        //     echo '<td>'. $match->$val->anex .'</td>';
                        // }
                        echo '<td>'. $match->declaration .'</td>';
                        echo '<td>'. $val.'</td>';
                        echo '<td>'. $match->status .'</td>';
                        echo '<td>'. $match->$val->class_description  .'</td>';
                        echo '<td>'. $match->$val->class_headings .'</td>';
                        echo '<td>'. $match->$val->matched .'</td>';
                        echo '<td>'. $match->$val->anex .'</td>';
                    }
                }
                
                ?>

                </tbody>
            </table>
        </div>
        @endif
    </div>
</div>
            
@stop