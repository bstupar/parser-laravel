            <table class="table table-striped" id="datatable">
                <thead>
                <tr>
                    <th>No.</th>
                    <th>EUTM No.</th>
                    <th>Registration date</th>
                    <th>Trademark name</th>
                    <th>Nice Edition</th>
                    <th>Nice Classification in</th>
                    <th>Eligible for declaration</th>
                    <th>Class</th>
                    <th>Complete/Incomplete</th>
                    <th>Class Text</th>
                    <th>Class Heading Text</th>
                    <th>Compared classes</th>
                    <th>Not covered by the literal meaning</th>
                </tr>
                </thead>
                <tbody>

                <?php
                $redni_br = 0;
                foreach ( $matches as $num => $match ) {

                    $redni_br++;
                    // loop po svakoj nice class i printuj ceo row
                    foreach ( $match[ 'niceclasses' ] as $val ) {

                        echo "<tr><td>{$redni_br}</td>";
                        echo "<td><a href='https://euipo.europa.eu/eSearch/#details/trademarks/{$num}'>TMN:" . $num . '</a></td>';
                        echo '<td>' . $match['regdate'].'</td>';
                        echo '<td>' . $match['trademark'] . '</td>';
                        echo '<td>' . $match['nice_edition'] . '</td>';
                        echo '<td>Edition ' . $match[ 'edition' ] . '</td>';
                        // we have full match
                        // if ( isset($match[ $val ]['full_match']) ) {
                        //     if ($match[ $val ]['full_match']) {
                        //         echo '<td>'. $match['declaration'] .'</td>';
                        //         echo '<td>'. $val.'</td>';
                        //         echo '<td>'. $match['status'] .'</td>';
                        //         echo '<td>'. $match[ $val ][ 'class_description' ] .'</td>';
                        //         echo '<td>'. $match[ $val ][ 'class_headings' ].'</td>';
                        //         echo '<td>'. $match[ $val ][ 'matched' ].'</td>';
                        //         echo '<td>'. $match[ $val ][ 'anex' ].'</td>';


                        //     }
                        //     // incomplete match
                        //     if ( $match[ $val ]['full_match'] == false) {
                        //         echo '<td>No</td>';
                        //         echo '<td>'. $val.'</td>';
                        //         echo '<td>Incomplete</td>';
                        //         echo '<td>' . App\Lib\Helper::compareStrColored( $match[ $val ][ 'matched' ], $match[ $val ][ 'class_description' ] ) . '</td>';
                        //         echo '<td>'.$match[ $val ][ 'class_headings' ].'</td>';
                        //     }
                        // } if (!isset( $match[ $val ]['full_match'] )) {
                        //     // not a match at all !
                        //     echo '<td>No</td>';
                        //     echo '<td>'. $val.'</td>';
                        //     echo '<td>Not matched</td>';
                        //     echo '<td>' .  $match[ $val ][ 'class_description' ]  . '</td>';
                        //     echo '<td>'.$match[ $val ][ 'class_headings' ].'</td>';
                        // }
                        echo '<td>'. $match['declaration'] .'</td>';
                        echo '<td>'. $val.'</td>';
                        echo '<td>'. $match['status'] .'</td>';
                        echo '<td>'. $match[ $val ][ 'class_description' ] .'</td>';
                        echo '<td>'. $match[ $val ][ 'class_headings' ].'</td>';
                        echo '<td>'. $match[ $val ][ 'matched' ].'</td>';
                        echo '<td>'. $match[ $val ][ 'anex' ].'</td>';
                    }
                }
                ?>

                </tbody>
            </table>