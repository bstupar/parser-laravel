@extends('layouts.dashboard')
@section('page_heading','Ohim Dashboard')
@section('section')

        <!-- /.row -->
<div class="col-sm-12">
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $pending }}</div>
                            <div>Na cekanju</div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-inbox fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $finished }}</div>
                            <div>Zavrsenih</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-download fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $working }}</div>
                            <div>U procesu obrade</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-search fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $ukupno_pretraga }}</div>
                            <div>Ukupno pretraga</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-8">

            @section ('pane2_panel_title', 'Responsive Timeline')
            @section ('pane2_panel_body')

                    <!-- /.panel -->



           <table class="table table-hover">
               <thead>
                   <tr>
                       <th>#</th>
                       <th>First Name</th>
                       <th>Last Name</th>
                       <th>Email</th>
                       <th>Option</th>
                   </tr>
               </thead>
               <tbody>
                   @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->last_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <button class="btn btn-outline btn-primary btn-xs" data-toggle="modal" data-target="#moduleModal{{ $user->id }}">Module permissions</button>
                        </td>
                    </tr>
                   @endforeach
               </tbody>
           </table>

            @foreach ($users as $user)
            <!-- modal -->
            <divl class="modal fade" id="moduleModal{{ $user->id }}" tabindex="-1" role="dialog" aria-labeledby="moduleModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title" id="moduleModalLabel">Users Options</h4>
                        </div>
                        <form action="POST" class="form">
                        <div class="modal-body">
                            
                                <div class="form-group">
                                    <label>Admin</label>
                                    <select name="admin" id="admin" class="form-control">
                                        <option value="1" {{ ($user->sudo === 1) ? 'selected' : ''}}>Yes</option>
                                        <option value="0" {{ ($user->sudo === 0) ? 'selected' : ''}}>No</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="modules">Modules</label>
                                    @foreach($modules as $module)
                                    <div class="checkbox">
                                        <label >
                                            <input type="checkbox" value="{{ $module->id }}"> {{ $module->name }}
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                            
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
                            <button class="btn btn-primary" type="submit">Save Changes</button>
                        </div>
                        </form>
                    </div>
                </div>
            </divl>
            @endforeach
            <!-- /.panel-body -->

            <!-- /.panel -->
            @endsection
            @include('widgets.panel', array('header'=>true, 'as'=>'pane2'))
        </div>
        <!-- /.col-lg-8 -->
        <div class="col-lg-4">

                        <!-- /.panel -->
            @section ('pane3_panel_title', 'Lista modula')
            @section ('pane3_panel_body')

        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <p>ovde ide tabela sa raspolozivim modulima tj njihovom listom</p>
        </div>
        <!-- /.panel-body -->
        <div class="panel-footer">
            <div class="input-group">
                <input id="btn-input" type="text" class="form-control input-sm" placeholder="Type your message here..." />
                                <span class="input-group-btn">
                                    <button class="btn btn-warning btn-sm" id="btn-chat">
                                        Dodaj
                                    </button>
                                </span>
            </div>
        </div>
        <!-- /.panel-footer -->
    </div>
    <!-- /.panel .chat-panel -->
    @endsection
    @include('widgets.panel', array('header'=>true, 'as'=>'pane3'))
</div>

<!-- /.col-lg-4 -->

@stop