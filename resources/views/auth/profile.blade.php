@extends('layouts.dashboard')
@section('page_heading','Profile')
@section('section')

<div class="col-sm-12">
    <div class="row">
        <div class="col-lg-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">User Details</h3>
                </div>
                <div class="panel-body">
                    <dl class="dl-horizontal">
                        <dt>Ime</dt>
                        <dd> {{ $name}} </dd>
                        <dt>Prezime</dt>
                        <dd> {{ $last_name}} </dd>
                        <dt>Email</dt>
                        <dd> {{ $email }} </dd>
                    </dl>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Aktivnosti</h3>
                </div>
                <div class="panel-body">
                    
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Datum</th>
                                <th>Aktivnost</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($history as $row)
                            <tr>
                                <td>{{ $row->datum }}</td>
                                <td>{{ $row->changed }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Change password</h3>
                </div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action("UserController@postChangePassword") }}" class="form-horizontal" method="post" role="form">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Password</label>
                            <divl class="col-md-7">
                                <input type="password" id="password" name="password" class="form-control" value autofocus>
                            </divl>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-7">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-7 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Change
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop