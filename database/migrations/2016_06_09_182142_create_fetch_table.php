<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFetchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fetch', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('request_id');
            $table->string('controller');
            $table->string('name');
            $table->string('number');
            $table->string('type');
            $table->string('imageurl');
            $table->string('filingdate');
            $table->string('nice');
            $table->string('status');
            $table->string('publisheddate');
            $table->string('publishedurl');
            $table->integer('applicantid');
            $table->string('applicantname');
            $table->integer('representativeid');
            $table->string('representativename');
            $table->index('request_id', 'request_id');
            $table->index('name', 'name');
            $table->index('number', 'number');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fetch');
    }
}
