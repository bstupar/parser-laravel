<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class ParserWatch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:watch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for Cron job to watch for new requests in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // check database if there is working thing ??
        $check = DB::table('cron_watch')->where('status', '=', 'working')->first();
        if (!$check) {
            // we dont have working job, let's see if we have some pending ?
            $pending = DB::table('cron_watch')->where('status', '=', 'pending')->first();
            if ($pending) {
                // if pending job exists, check what type of job is it
                $request = DB::table('requests')->where('id', '=', $pending->request_id)->first();

                // writte in database that we took that job !
                DB::table('cron_watch')->where('request_id', '=', $pending->request_id)->update(['status' => 'working']);

                // based on what request type is, call specific command and pass id and parameters for search
                switch ($request->type) {
                    case ('name'):
                        $this->call('parser:name', [
                            'name' => $request->term,
                            '--request' => $request->id
                            ]);
                        break;
                    case ('owner_id'):
                        $this->call('parser:owner', [
                            'id' => $request->term,
                            '--request' => $request->id
                            ]);
                        break;
                    case ('tm_number'):
                      //dd($request->term.PHP_EOL.$request->id);
                        $this->call('parser:searchnumber', [
                            'number' =>  unserialize($request->term),
                            '--request' => $request->id
                            ]);
                        break;
                }

            }
        }
    }
}
