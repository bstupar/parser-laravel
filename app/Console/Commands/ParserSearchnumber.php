<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class ParserSearchnumber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:searchnumber {number*} {--request=} {--retry=?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for Cron job that does search for numbers specified';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // script takes some time so ..
        set_time_limit( 0 );

        // get passed arguments
        $number = $this->argument('number');
        $request_id = $this->option('request');

        // update cron watch table where we are at the moment (mostly for debuging)
        DB::table( 'cron_watch' )->where( 'request_id', $request_id )->update( [ 'stage' => 'Search Numbers' ] );

        $url = 'https://euipo.europa.eu/copla/ctmsearch/json';

        for ($i = 0; $i < count($number); $i++) {

          // counter
          $start = 0;

          for ( $j = 0; $j <= $start; $j++ ) {

            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13' );
            curl_setopt( $ch, CURLOPT_POST, 1 );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, "start={$start}&rows=100&searchMode=advanced&criterion_1=ApplicationNumber&term_1={$number[$i]}&operator_1=AND&condition_1=CONTAINS&sortField=ApplicationNumber&sortOrder=asc" );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

            $output = curl_exec( $ch );
            // if request fails for some reason, let's retry it
            if ( $output === false ) {

                $retry = (int) $this->option( 'retry' );
                // we got 3 failed attempts let's writte that in DB
                if ( isset( $retry ) && $retry > 3 ) {
                    DB::table( 'fetch_errors' )->insert( [
                        'request_id' => $request_id,
                        'term'       => $name,
                        'error'      => curl_error( $ch )
                    ] );
                    DB::table( 'cron_watch' )->where( 'request_id', $request_id )->update( [ 'stage' => 'Failed SearchNumber' ] );
                    exit();
                }
                // if there is some error while processing this form
                // wait 1 min and try again
                sleep( 60 );
                $this->call( 'parser:searchnumber', [
                    'number'      => $number,
                    '--request' => $request_id,
                    '--retry'   => $retry + 1
                ] );

                exit();
            } else {
                $temp_obj = json_decode( $output, true );

                if ( (int) $temp_obj['total'] == 0 ) {
                  continue;
                }

                foreach ( $temp_obj[ 'items' ] as $key => $val ) {
                    DB::table( 'fetch' )->insert( [
                        'request_id'         => $request_id,
                        'controller'         => $val[ 'controller' ],
                        'name'               => $val[ 'name' ],
                        'number'             => $val[ 'number' ],
                        'type'               => $val[ 'type' ],
                        'imageurl'           => $val[ 'imageurl' ],
                        'filingdate'         => isset( $val[ 'filingdate' ] ) ? $val[ 'filingdate' ] : '',
                        'nice'               => $val[ 'nice' ],
                        'status'             => $val[ 'status' ],
                        'publisheddate'      => isset( $val[ 'publisheddate' ] ) ? $val[ 'publisheddate' ] : '',
                        'publishedurl'       => isset( $val[ 'publishedurl' ] ) ? $val[ 'publishedurl' ] : '',
                        'applicantid'        => isset( $val[ 'applicantid' ] ) ? $val[ 'applicantid' ] : '',
                        'applicantname'      => isset( $val[ 'applicantname' ] ) ? $val[ 'applicantname' ] : '',
                        'representativeid'   => $val[ 'representativeid' ],
                        'representativename' => $val[ 'representativename' ]
                    ] );
                }
                if ( count( $temp_obj[ 'items' ] ) == 100 ) {
                    $start = $start + 100;
                } else {
                    $start = 0;
                }
            }

            curl_close( $ch );
            sleep( 3 );
          }
        }

        // when script finish with scraping data in the database start second script
        $fetch_numbers = DB::table( 'fetch' )->select( 'number' )->where( 'request_id', '=', $request_id )->get();
        $num_arr = [ ];
        foreach ( $fetch_numbers as $num ) {
          $num_arr[] = $num->number;
        }

        $this->call( 'parser:number', [
            'number'    => $num_arr,
            '--request' => $request_id
        ] );

    }
}
