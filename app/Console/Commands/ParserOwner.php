<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class ParserOwner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:owner {id} {--request=} {--retry=?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for Cron job to parse reuqest by trademark owner id - module 28.8';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //script takes some time so ..
        set_time_limit( 0 );

        // get passed arguments
        $id = (string) $this->argument('id');
        $request_id = (string) $this->option('request');

        // update cron watch table for debugin and progress tracking
        DB::table('cron_watch')->where('request_id', '=', $request_id)->update(['stage' => 'Owner ID Parsing']);
        // base post url
        $api_url = "https://euipo.europa.eu/copla/ctmsearch/json";

        // counter
        $start = 0;

        for ( $i = 0; $i <= $start; $i++ ) {
            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, $api_url );
            curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13' );
            curl_setopt( $ch, CURLOPT_POST, 1 );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, "start={$start}&rows=100&searchMode=basic&criterion_1=ApplicantIdentifier&term_1={$id}&operator_1=AND&sortField=ApplicationNumber&sortOrder=asc" );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );


            $output = curl_exec( $ch );
            if ($output === false ) {
                $retry = (int) $this->option('retry');
                if (isset($retry) && $retry > 3) {
                    DB::table( 'fetch_errors' )->insert( [
                        'request_id' => $request_id,
                        'term'       => $id,
                        'error'      => curl_error( $ch )
                    ] );
                    DB::table( 'cron_watch' )->where( 'request_id', $request_id )->update( [ 'stage' => 'Failed SearchOwner' ] );
                    exit();
                }
                // if there is some error while processing this form
                // wait 1 min and try again
                sleep( 60 );
                $this->call('parser:owner', [
                    'id' => $id,
                    '--request' => $request_id,
                    '--retry' => $retry + 1
                ]);
                exit();
            } else {
                $temp_obj = json_decode( $output, true );


                foreach ( $temp_obj[ 'items' ] as $key => $val ) {
                  DB::table( 'fetch' )->insert( [
                      'request_id'         => $request_id,
                      'controller'         => $val[ 'controller' ],
                      'name'               => $val[ 'name' ],
                      'number'             => $val[ 'number' ],
                      'type'               => $val[ 'type' ],
                      'imageurl'           => $val[ 'imageurl' ],
                      'filingdate'         => isset( $val[ 'filingdate' ] ) ? $val[ 'filingdate' ] : '',
                      'nice'               => $val[ 'nice' ],
                      'status'             => $val[ 'status' ],
                      'publisheddate'      => isset( $val[ 'publisheddate' ] ) ? $val[ 'publisheddate' ] : '',
                      'publishedurl'       => isset( $val[ 'publishedurl' ] ) ? $val[ 'publishedurl' ] : '',
                      'applicantid'        => isset( $val[ 'applicantid' ] ) ? $val[ 'applicantid' ] : '',
                      'applicantname'      => isset( $val[ 'applicantname' ] ) ? $val[ 'applicantname' ] : '',
                      'representativeid'   => $val[ 'representativeid' ],
                      'representativename' => $val[ 'representativename' ]
                  ] );
                }

                if ( count( $temp_obj[ 'items' ] ) == 100 ) {
                    $start = $start + 100;
                } else {
                    $start = 0;
                }
            }

            curl_close( $ch );

            sleep( 3 );
        }

         // when script finish with scraping data in the database start second script
        $numbers = DB::table('fetch')->select('number')->where('request_id', '=', $request_id)->get();
        $number = [];
        foreach ($numbers as $num) $number[] = $num->number;

        // call next script
        $this->call('parser:number', [
            'number' => $number,
            '--request' => $request_id
            ]);
    }
}
