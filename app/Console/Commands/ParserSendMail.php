<?php

namespace App\Console\Commands;

use Mail;
use DB;
use Illuminate\Console\Command;

class ParserSendMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:sendmail {--request=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for Cron job to send email notification for finished job - module 28.8';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $request_id = (string) $this->option('request');

         // get email address of user who set the request
        $user = DB::table('users')
                    ->join('requests', 'users.id', '=','requests.user_id')
                    ->select('users.email', 'users.name', 'users.last_name')
                    ->where('requests.id', '=', $request_id)
                    ->first();

        Mail::send('auth.emails.finished', ['user' => $user, 'request' => $request_id], function ($m) use ($user)  {
            $m->from('msa.extraction@gmail.com', 'Ohim No-Reply');

            $m->to($user->email, $user->name . ' ' . $user->last_name )->subject('Zavrsena pretraga');
        });

        DB::table('cron_watch')
            ->where('request_id', '=', $request_id)
            ->update(['stage' => 'Mail sent']);
    }
}
