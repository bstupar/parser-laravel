<?php

namespace App\Console\Commands;

use DB;
use Storage;
use Excel;
use Illuminate\Console\Command;
use App\Lib\JSONParser;
use App\Lib\Helper;

class ParserProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:process {--request=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for Cron job to process collected data and build report - module 28.8';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // this takes a lot of time and it comsumes a lot of memory
        ini_set('memory_limit', '-1');
        set_time_limit( 0 );

        // get passed option argument
        $request_id = (string) $this->option('request');

        // update cron watch table for debugin and progress tracking
        DB::table('cron_watch')->where('request_id', '=', $request_id)->update(['stage' => 'Processing data']);
        // count number of records in database
        $count = DB::table('scraps')->where('request_id', '=', $request_id)->count();

        $matches = [ ];

        for ( $i = 0; $i < $count; $i++ ) {
            // we iterate over counted number of rows in db where we have $request_id same
            // and take 1 by 1 row at the time with limit nad offset, in this case, skip and take
            $row = DB::table( 'scraps' )->select( 'json', 'number' )->where( 'request_id', '=', $request_id )->skip($i)->take(1)->first();

            // make JSONParser instance and pull relevant data from the provided json string
            $jsonparser = new JSONParser( $row->json );
            // return filling date from provided json and compare if date is befor 22-06-2012
            // returns bool
            $filling_date = $jsonparser->getFillingDate();
            $registered = $jsonparser->isRegistered();
            $year = $jsonparser->getYear();
            $tmName = $jsonparser->getTmName();
            //echo "F: {$filling_date} - Y: {$year} - R: {$registered} \n\r";

            // we need data only from 1997 to 2012 including 2012
            if ( $filling_date && $year >= 1997 && $registered ) {

                $regDate = $jsonparser->regDate();
                $appDate = $jsonparser->appDate();

                $edition = DB::table( 'nice_editions' )->select( 'id', 'edition' )->where( 'year_from', '<=', $year )
                             ->where( 'year_to', '>=', $year )->first();

                $nice_class = $jsonparser->getNiceClass();
                $class_description = $jsonparser->getClassDescription();

                // loop through nice_classes and fetch the data from the database
                $class_headings = [ ];

                // get class_heading from database and do comparison
                foreach ( $nice_class as $class ) {
                    $query = DB::table( 'nice_editions_class_headings' )
                               ->select( 'class_headings' )
                               ->where( 'nice_editions_id', '=', $edition->id )
                               ->where( 'class', '=', $class )
                               ->first();

                    // can be null !
                    if ( $query ) {
                        $class_headings[ $class ] = $query->class_headings;
                        if ( !empty( $class_headings[ $class ] ) ) {
                            // check if we have full match with provided strings
                            $full_match = Helper::checkFullMatch( $class_headings[ $class ], $class_description[ $class ] );
                            $anex = DB::table('nice_editions_class_anex')->select('goods')->where('nice_editions_id', '=', $edition->id)->where('class', '=', $class)->first();


                            if ( $full_match === true) {
                                // build result array  for parsing
                                $matches[ $row->number ][ 'edition' ] = $edition->id;
                                $matches[ $row->number ][ 'niceclasses' ][] = $class;
                                $matches[ $row->number ][ 'regdate' ] = 'App. date: '. $appDate . ' / Reg. date: '. $regDate;
                                $matches[ $row->number ][ 'nice_edition' ] = $edition->edition;
                                $matches[ $row->number ][ 'trademark' ] = $tmName;
                                $matches[ $row->number ][ 'declaration' ] = 'Yes / Egible';
                                $matches[ $row->number ][ 'status' ] = 'Yes / Egible';
                                $matches[ $row->number ][ $class ][ 'anex' ] = $anex->goods;
                                $matches[ $row->number ][ $class ][ 'matched' ] = Helper::colorMatch( $class_headings[ $class ], $class_description[ $class ] );
                                $matches[ $row->number ][ $class ][ 'class_headings' ] = $class_headings[ $class ];
                                $matches[ $row->number ][ $class ][ 'full_match' ] = $full_match;
                                $matches[ $row->number ][ $class ][ 'class_description' ] = $class_description[ $class ];
                            } else {

                                // check parts since we don't have full match and see if it's incomplete or not a match
                                $compare = Helper::compareStrStr( $class_headings[ $class ], $class_description[ $class ] );
                                if ( $compare ) {
                                    $matches[ $row->number ][ 'edition' ] = $edition->id;
                                    $matches[ $row->number ][ 'niceclasses' ][] = $class;
                                    $matches[ $row->number ][ 'regdate' ] = 'App. date: '. $appDate . ' / Reg. date: '. $regDate;
                                    $matches[ $row->number ][ 'nice_edition' ] = $edition->edition;
                                    $matches[ $row->number ][ 'trademark' ] = $tmName;
                                    $matches[ $row->number ][ 'declaration' ] = 'Yes';
                                    $matches[ $row->number ][ 'status' ] = 'Class heading is contained in class text';
                                    $matches[ $row->number ][ $class ][ 'anex' ] = $anex->goods;
                                    $matches[ $row->number ][ $class ][ 'class_headings' ] = $class_headings[ $class ];
                                    $matches[ $row->number ][ $class ][ 'full_match' ] = $full_match;
                                    $matches[ $row->number ][ $class ][ 'class_description' ] = $class_description[ $class ];
                                    $matches[ $row->number ][ $class ][ 'matched' ] = Helper::colorMatch( $class_headings[ $class ], $class_description[ $class ] );

                                } else {
                                    // its not a match at all ! Build an array with info for displaying
                                    $matches[ $row->number ][ 'edition' ] = $edition->id;
                                    $matches[ $row->number ][ 'niceclasses' ][] = $class;
                                    $matches[ $row->number ][ 'regdate' ] = 'App. date: '. $appDate . ' / Reg. date: '. $regDate;
                                    $matches[ $row->number ][ 'nice_edition' ] = $edition->edition;
                                    $matches[ $row->number ][ 'trademark' ] = $tmName;
                                    $matches[ $row->number ][ 'declaration' ] = 'No';
                                    $matches[ $row->number ][ 'status' ] = 'Probably not eligible for Declaration of 28.8 EUTMR: Some terms are repeted, beign the repetition limited.';
                                    $matches[ $row->number ][ $class ][ 'anex' ] = $anex->goods;
                                    $matches[ $row->number ][ $class ][ 'matched' ] = Helper::colorMatch( $class_headings[ $class ], $class_description[ $class ] );
                                    $matches[ $row->number ][ $class ][ 'class_headings' ] = $class_headings[ $class ];
                                    $matches[ $row->number ][ $class ][ 'class_description' ] = $class_description[ $class ];
                                }
                            }
                        }
                    }
                }
            }
            unset($row);
            unset($edition);
            unset($query);
            unset($jsonparser);
        }

        // it's all done ! FINALY !
        // Let's update database and send an email to the user and create json file with the results

        // generates excel file !
         Excel::create('pretraga_'.$request_id, function($excel) use ($request_id, $matches){
            $excel->setTitle('Rezultat pretrage')
                    ->setCreator('Ohim app')
                    ->setCompany('msa iplaw')
                    ->setDescription('Rezultat pretrage');


            $excel->sheet('First sheet', function($sheet) use ($request_id, $matches) {
                $sheet->setAutoSize(true);

                $sheet->loadView('excel', ['matches' => $matches ] );
            });

        })->store('xlsx', public_path('excel/'));

        // convert results in json so we can save it
        $content = json_encode($matches);

        // before saving, check if directory exists, and if not create one
        $directories = Storage::directories();
        if (!in_array('reports', $directories)) Storage::makeDirectory('reports');

        // save finished json in json file
        Storage::put('reports/request_'. $request_id .'.json', $content);

        // update cron job db that we're done
        DB::table('cron_watch')
            ->where('request_id', '=', $request_id)
            ->update(['status' => 'finished']);

        // update request table that we are finished
        DB::table('requests')
            ->where('id', '=', $request_id)
            ->update([
                'status' => 1,
                'izvestaj_path' => 'excel/pretraga_'. $request_id .'.xlsx',
                'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
                ]);

        // call send email cli to notify user that we have finished !
        $this->call('parser:sendmail', [
            '--request' => $request_id
            ]);

    }
}
