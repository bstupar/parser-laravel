<?php

namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

class ParserNumber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:number {number*} {--request=} {--retry=?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for Cron job to fetch single jsons by number specified';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //script takes some time so ..
        set_time_limit( 0 );

        // get passed arguments
        $number = $this->argument('number');
        $request_id = $this->option('request');

        // update cron watch table for debugin and progress tracking
        DB::table('cron_watch')->where('request_id', '=', $request_id)->update(['stage' => 'Number Parsing']);

        $api_url = "https://euipo.europa.eu/copla/trademark/data/withOppoRelations/";


        for ( $i = 0; $i < count( $number ); $i++ ) {


            $url = $api_url . $number[ $i ];

            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13' );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

            $output = curl_exec( $ch );
            //handle some errors if curl doesn't pass
            if ($output === false) {
                $retry = (int) $this->option('retry');
                if (isset($retry) && $retry > 3) {
                    DB::table( 'fetch_errors' )->insert( [
                        'request_id' => $request_id,
                        'term'       => $number[ $i ],
                        'error'      => curl_error( $ch )
                    ] );
                    DB::table( 'cron_watch' )->where( 'request_id', $request_id )->update( [ 'stage' => 'Failed Number Parsing' ] );
                    exit();
                }
                // if there is some error while processing this form
                // wait 1 min and try again
                sleep( 60 );
                $this->call('parser:number', [
                    'number' => $number,
                    '--request' => $request_id,
                    '--retry' => $retry + 1
                ]);
                exit();
            } else {
                // store data in db
                DB::table( 'scraps' )->insert( [
                    'request_id' => $request_id,
                    'number'  => $number[ $i ],
                    'json'    => $output
                ] );
            }




            curl_close( $ch );
            // wait time is 3 seconds (we don't want to flood their api with requests)
            sleep( 3 );
        }
        // sledeca scripta obrada podataka i generisanje ! (novartis.php call)
        $this->call('parser:process', [
            '--request' => $request_id
            ]);
    }
}
