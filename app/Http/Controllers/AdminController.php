<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
	public function __construct() 
	{
		$this->middleware('admin');
	}

	/**
	 * Display admin page with some data
	 * 
	 */
	public function showIndex()
	{
		// get current number of tasks pending. working, finished
		$data['pending'] = DB::table('cron_watch')->where('status', '=', 'pending')->count();
		$data['working'] = DB::table('cron_watch')->where('status', '=', 'working')->count();
		$data['finished'] = DB::table('cron_watch')->where('status', '=', 'finished')->count();
		$data['ukupno_pretraga'] = DB::table('cron_watch')->count();
		
//		if ( $data['working'] === 1) {
//			$request_id = DB::table('cron_watch')->select('request_id')->where('status', '=', 'working')->first();
//			$data['work_details'] = DB::table('users')
//								->join('requests', 'users.id', '=','requests.user_id')
//			                    ->select('users.email', 'users.name', 'users.last_name', 'requests.term', 'requests.type')
//			                    ->where('requests.id', '=', $request_id)
//			                    ->first();
//		}
		$data['users'] = DB::table('users')->get();
		$data['modules'] = DB::table('modules')->get();
		return view('admin.home', $data);
	}


	/**
	 * Show edit user page
	 * 
	 */
	public function showEditUser( $id )
	{
		
		$data['user'] = DB::table('users')->where('id', '=', $id)->first();
		
		return view('admin.userprofile', $data);
	}

	private function getUserMembership( $id )
	{
		DB::table('');
	}
	/**
	 * Process edited user data
	 * @param  Request $request 
	 * @param  int  $id      
	 */
	public function postEditUser( Request $request, $id)
	{
		$validation_rules = [
			'name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'admin' => 'required|numeric'
				];
		$validator = Validator::make($request->all(), $validation_rules);

		if($validator->fails()) {
			return redirect()->back()->withErrors($validator)->withInput();
		}
		
		DB::table('users')->where('id', '=', $id)->update([
			'name' => $request->name,
			'last_name' => $request->last_name,
			'email' => $request->email,
			'sudo' => $request->admin
			]);

		DB::table('user_history')->insert([
			'user_id' => $id,
            'changed' => 'Administrator je izvrsio izmenu profila',
            'datum' => \Carbon\Carbon::now()->toDateTimeString()
            ]);

		return redirect('admin')->with('status', 'Uspesno izmenjen profil korisnika');

	}


	/**
	 * Process adding new module
	 * 	
	 * @param Request $request 
	 */
	public function addModule(Request $request)
	{
		$validation_rules = [
			'name' => 'required|regex:/^[\w\-\_\'\s]+$/',
			];
		$message = [
			'regex' => 'Dozvoljeni karakteri su velika i mala slova, brojevi, razmaci, -, _, \''
			];
		$validator = Validator::make($request->all(), $validation_rules, $message);

		if ( $validator->fails() ) {
			return redirect()->back()->withErrors($validator)->withInput();
		}

		DB::table('modules')->insert([
			'name' => $request->name
			]);
		return redirect()->back()->with('status', 'Uspesno dodat nov modul');
	}


}
