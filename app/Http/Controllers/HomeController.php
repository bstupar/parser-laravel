<?php

namespace App\Http\Controllers;

use DB;
use Excel;
use Storage;
use App\Http\Requests;
use App\Lib\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function __construct()
    {
       $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pretraga');
    }


    public function dashboard()
    {
        return view('home');
    }


    public function forms()
    {
        return view('form');
    }


    public function documentation()
    {
        return view('documentation');
    }

    public function test()
    {
        $json = DB::table( 'scraps' )->select( 'json' )->where( 'number', '=', '005225041' )->first();
        $json = json_decode($json->json);
        dd($json->entity->name);
    }
}
