<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class DebugController extends Controller
{
    


	public function displayCronQue()
	{
		$que = DB::table('cron_watch')->take(10)->get();

		return view('debug', $que);
	}

}
