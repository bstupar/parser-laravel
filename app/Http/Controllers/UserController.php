<?php

namespace App\Http\Controllers;

use DB;
use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showProfile()
    {
        $user = Auth::user();
        $data = [
            'name' => $user->name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'history' => DB::table('user_history')->where('user_id', '=', Auth::user()->id)->get()
        ];

        return view('auth.profile', $data);
    }
    
    public function postChangePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
                        'password' => 'required|min:6|confirmed',
                    ]);

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }


        $user = User::find(Auth::user()->id);
        $user->password = bcrypt($request->password);
        $user->save();
        DB::table('user_history')->insert([
            'user_id' => Auth::user()->id,
            'changed' => 'Promenjen password',
            'datum' => \Carbon\Carbon::now()->toDateTimeString()
            ]);

        return redirect()->back()->with('status', 'Password uspesno promenjen');

    }
}
