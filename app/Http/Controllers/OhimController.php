<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Storage;
use Illuminate\Http\Request;
use App\Models\Request as Req;
use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class OhimController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showPretraga()
    {
        return view('pretraga');
    }

    /**
     *  Obrada forme za pretragu
     *  
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function pretraga(Request $request)
    {
        // validate input
        $validator = $this->pretragaValidator( $request );

        // ako ne prodje validaciju, vratiti nazad i prikazati greske
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }


        // check term since it can be string or array
        $term = (is_array($this->checkTerm( $request )) ? serialize( $this->checkTerm( $request ) ) : $this->checkTerm( $request ));
        $readable_term =  (is_array($this->checkTerm( $request )) ? implode( ' ', $this->checkTerm( $request ) ) : $this->checkTerm( $request ));
        
        // proveriti u bazi da li postoji trazeni term
        $pretraga = Req::where('term', '=', $term )->get();

        if ($pretraga->count() > 0) {
            // ako postoji vratiti nazad rezultat(e)
            return redirect()->back()->with('pretraga', $pretraga);
        } else {
               
            // ako ne postoji, upisati u bazu request i pending cron job
            $request_id = DB::table('requests')->insertGetId([
                            'user_id' => Auth::user()->id,
                            'type'  => $request->type,
                            'term' => $term,
                            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                            'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
                            ]);
            DB::table('cron_watch')->insert([
                'request_id' => $request_id,
                'status'    => 'pending'
                ]);
            DB::table('user_history')->insert([
                'user_id' => Auth::user()->id,
                'changed' => 'Pretraga po: ' . $this->pretragaType( $request ) . ' , Parametar: ' . $readable_term,
                'datum' => \Carbon\Carbon::now()->toDateTimeString()
                ]);
            return redirect('/')->with('status', 'Uspesno poslat zahtev. Bicete obavesteni putem emaila kada zahtev bude obradjen');
        } 
    }

    /**
     * Return string instead of field name
     * 
     * @param  Request $request 
     * @return string           
     */
    private function pretragaType( Request $request)
    {
        switch ($request->type) {
            case ('name'):
                return 'Imenu';
                break;
            case ('owner_id'):
                return 'Owner ID-u';
                break;
            case ('tm_number'):
                return 'Trademark Broju';
                break;
        }
    }

    /**
     * Checks what type is and do validation for it
     *  
     * @param  string $term 
     * @param  string $type 
     * @return bool|mixed
     */
    protected function pretragaValidator( Request $request)
    {
        $messages = [
            'required' => 'Polje je obavezno',
            'numeric' => 'Polje prihvata samo brojeve (bez razmaka)',
            'regex' => 'Dozvoljeni karakteri su velika i mala slova, brojevi, razmaci, -, _, \''
        ];
        if ($request->type === 'name') {
            return Validator::make($request->all(), [ 'term' => 'required|regex:/^[\w\-\_\'\s]+$/' ], $messages);
        } if ($request->type === 'owner_id') {
            return Validator::make($request->all(), [ 'term' => 'numeric|required' ], $messages);
        } if ($request->type === 'tm_number') {
            return Validator::make($request->all(), [ 'term' => 'required|regex:/^[0-9\s]+$/' ], $messages);
        }
    }

    /**
     * Checks term that's passed to the method pretraga()
     * 
     * @param  string $term
     * @param  string $type
     * 
     * @return string|array
     */
    protected function checkTerm( Request $request )
    {
        if ($request->type === 'name') return (string) trim($request->term);
        if ($request->type === 'owner_id') return (string) trim($request->term);
        if ($request->type === 'tm_number') {
            // check if tere are new lines in string
            // if so replace them with spaces then explode by space and return array
            // or explode by space character and return array
            // no need for extra escaping since at first it needs to pass validation
            if ( strstr( $request->term, "\r\n")) {
                $term = str_replace("\r\n", ' ', $request->term);
                return explode( ' ', trim($term));

            } else {
                return explode( ' ', trim($request->term));
            }
        }
    }


    /**
     * Display results 
     *         
     * @param  int $request_id 
     * @return view obj
     */
    public function displayResult( $request_id )
    {
        $content = Storage::has('reports/request_'. $request_id .'.json');
        
        if ($content) {

            $request = DB::table('requests')
                        ->where('id', '=', $request_id)
                        ->first();
            return view('rezultat', ['matches' => json_decode(Storage::get('reports/request_'. $request_id .'.json')), 'request' => $request ] );
        } else {
            abort(404, 'Trazeni rezultat nije pronadjen. Ukoliko mislite da je greska, kontaktirajte administartora.');
        }
    }

    public function downloadExcel( $request_id )
    {
        $file = DB::table('requests')->select('izvestaj_path')->where('id', '=', $request_id)->first();
        return response()->download($file->izvestaj_path);
    }

    
}
