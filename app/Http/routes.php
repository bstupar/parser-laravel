<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// auth
Route::auth();

// user profile
Route::get('user/profile', 'UserController@showProfile');
Route::post('user/passchange', 'UserController@postChangePassword');

// module ohim 28.8
Route::get('/', 'OhimController@showPretraga');
Route::post('pretraga', "OhimController@pretraga");
Route::get('pretraga/{id}', "OhimController@displayResult");
Route::get('download/{id}', ['as' => 'download', 'uses' => 'OhimController@downloadExcel']);

// admin routes
Route::get('/admin', "AdminController@showIndex");
Route::get('admin/edituser/{id}', "AdminController@showEditUser");
Route::post('admin/edituser/{id}', "AdminController@postEditUser");
Route::post('admin/addmodule', "AdminController@addModule");




Route::get('/test', 'HomeController@test');