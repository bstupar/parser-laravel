<?php
/**
 * JSON parser class used to pull data from json strings
 * @author Bosko Stupar <bosko.stupar@gmail.com>
 */
namespace App\Lib;

class JSONParser
{

private $json_str = '';

    public function __construct( $json )
    {
        $this->json_str = json_decode( $json, true );
    }

    /**
     * Get registration date
     *
     * @return string
     */
    public function regDate()
    {
        $date = date( "d-m-Y", $this->json_str[ 'entity' ][ 'regdate' ]/1000 );

        return $date;
    }

    /**
     * Get application date
     * 
     * @return string
     */
    public function appDate()
    {
        if ( empty( $this->json_str[ 'entity' ]['filingdate'] ) ) {

            return 'Not set';
        } 

        return date( "d-m-Y", $this->json_str[ 'entity' ][ 'filingdate' ]/1000 );
    }

    /**
     * Get Year string from json
     *
     * @return bool
     */
    public function getYear()
    {
        if (empty($this->json_str[ 'entity' ][ 'filingdate' ])) {
            return date( "Y", $this->json_str[ 'entity' ][ 'regdate' ]/1000 );
        } else {
            return  date( "Y", $this->json_str[ 'entity' ][ 'filingdate' ]/1000 );    
        }
        
    }

    /**
     * @return bool
     */
    public function isRegistered()
    {
        $status = $this->json_str['entity']['status'];
        if (isset($status) && $status === 'Registered' OR isset($status) && $status === 'IR accepted') {
            return true;
        }
        else {
            return false;
        }

    }

    /**
     * Get nice class values from json
     *
     * @return array
     */
    public function getNiceClass()
    {
        $nice_class = $this->json_str[ 'entity' ][ 'niceclasses' ];

        return $nice_class;
    }

    /**
     * Get class description and number values from json
     *
     * @return array
     */
    public function getClassDescription()
    {
        $class_description = [ ];
        $arr = $this->json_str[ 'entity' ][ 'gs' ][ 'defaultValue' ][ 'values' ];

        foreach ( $arr as $key ) {
            $class_description[ $key[ 'number' ] ] = $key[ 'value' ];
        }

        return $class_description;
    }

    /**
     * Check if date from json (registered date) is before declaration 28-08-2012
     *
     * @return bool
     */
    public function getFillingDate()
    {
        
        if (empty($this->json_str[ 'entity' ][ 'filingdate' ])) {
            $filling_date = date( "d-m-Y", $this->json_str[ 'entity' ][ 'regdate' ]/1000 );
        } else {
            $filling_date = date( "d-m-Y", $this->json_str[ 'entity' ][ 'filingdate' ]/1000 );    
        }

        $declaration = '22-06-2012';

        return (strtotime($filling_date) < strtotime($declaration));
    }
    public function getTmName()
    {
        return $this->json_str['entity']['name'];
    }
}