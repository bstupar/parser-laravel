<?php
/**
 * Parser for specific html input
 * @author Bosko Stupar <bosko.stupar@gmail.com>
 *
 */
namespace App\Lib;

class Parser {

    private $strong_pattern         = '/<strong>(.*)<\/strong>/iU';
    private $valid_input_pattern    = '/<ul class=\\"([^\\"]*)\\">(.*)<\/ul>/iUs';
    private $gors_nodes_pattern     = '/<li class=\"([^\"]*)\">(.*)<\/li>/iUs';
    private $num_pattern            = '/<div.*class\s*=\s*["\'].*basic_numbers.*["\']\s*>(.*)<\/div>/iUs';
    private $taxonomy_pattern       = '/<div.*class\s*=\s*["\'].*taxonomy_node_text.*["\']\s*>(.*)<\/div>/iUs';
    private $data                   = '';

    /**
     * Constructor
     *
     * @param $data
     */
    public function __construct( $data )
    {
        $this->data = $data;
    }

    /**
     * Check if input is valid (if it has open and closed <ul> tag)
     *
     * @return bool
     */
    public function checkInput()
    {
        $result = preg_match_all( $this->valid_input_pattern, $this->data );

        if ( $result == 1 ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check and reformat data and return the result
     *
     * @return array|string
     */
    public function getTable()
    {
        if ( $this->checkInput() ) {
            $gors = $this->getGors();

            if ( $gors ) {
                $result = [];
                foreach ( $gors as $node ) {
                    $result[ $this->getNumField( $node ) ] = $this->getNameFiled( $node );
                }
                return $result;
            }
        } else {
            return "Uneti podaci nisu validni ili je pogresan encoding!";
        }

    }

    /**
     * Get li class="gors" node
     *
     * @return array | bool
     */
    private function getGors()
    {
        preg_match_all( $this->gors_nodes_pattern, $this->data, $output_lines );

        if ( is_array($output_lines) && !empty( $output_lines[2] ) ) {
            return $output_lines[2];
        } else {
            return false;
        }
    }

    /**
     * Get number field
     *
     * @param $str
     * @return string
     */
    private function getNumField( $str )
    {
        preg_match_all( $this->num_pattern, $str, $output);

        if ( is_array( $output ) && !empty( $output[1] ) ) {
            return $output[1][0];
        } else {
            return "";
        }
    }

    /**
     * Get name field and remove strong pattern
     *
     * @param $str
     * @return string
     */
    private function getNameFiled( $str )
    {
        preg_match_all( $this->taxonomy_pattern, $str, $output);

        if ( is_array( $output ) && !empty( $output[1] ) ) {
            return $this->removeStrongTag( $output[1][0] );
        } else {
            return "";
        }
    }

    /**
     * Remove <strong> tag from the string
     *
     * @param $str
     * @return string
     */
    private function removeStrongTag( $str )
    {
        return preg_replace( $this->strong_pattern, "$1", $str);
    }
}