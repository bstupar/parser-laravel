<?php
/**
 * Class for CURL fetching and handling those errors !
 *
 * @author Bosko Stupar <bosko.stupar@gmail.com>
 */

namespace App\Lib;

class Fetch {

	public function checkNameRequest( $name )
    {

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, "http://euipo.europa.eu/copla/ctmsearch/json" );
        curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13' );
        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, "start=0&rows=100&searchMode=advanced&criterion_1=ApplicantName&term_1={$name}&operator_1=AND&condition_1=CONTAINS&sortField=ApplicationNumber&sortOrder=asc" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );


        $output = curl_exec( $ch );
        $info = curl_getinfo( $ch );
        $temp_obj = json_decode( $output, true );
        $items = count( $temp_obj[ 'items' ] );
        curl_close( $ch );

        if ($items > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function checkOwnerRequest( $id )
    {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, "https://euipo.europa.eu/copla/ctmsearch/json" );
        curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13' );
        curl_setopt( $ch, CURLOPT_POST, 1 );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, "start=0&rows=100&searchMode=basic&criterion_1=ApplicantIdentifier&term_1={$id}&operator_1=AND&sortField=ApplicationNumber&sortOrder=asc" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

        $output = curl_exec( $ch );
        $info = curl_getinfo( $ch );
        $temp_obj = json_decode( $output, true );
        $items = count( $temp_obj[ 'items' ] );
        curl_close( $ch );

        if ($items > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function checkNumberRequest( array $number )
    {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, 'https://euipo.europa.eu/copla/trademark/data/withOppoRelations/'. $number[ $i ] );
        curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13' );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );

        $output = curl_exec( $ch );
        $info = curl_getinfo( $ch );
        $temp_obj = json_decode( $output, true );
        $items = count( $temp_obj[ 'items' ] );
        curl_close( $ch );

        if ($items > 0) {
            return true;
        } else {
            return false;
        }
    }

}