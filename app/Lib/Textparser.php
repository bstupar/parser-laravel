<?php

/**
 * Read and parse text files. Used with Parser class
 * @author Bosko Stupar <bosko.stupar@gmail.com>
 *
 */
namespace App\Lib;

use App\Lib;


class Textparser
{

    private $file = '';
    private $file_dir = '';

    public function __construct( $file, $file_dir )
    {
        $this->file_dir = $file_dir;
        $this->file     = $file;
    }

    /**
     * Parse content of a file
     *
     * @return array|bool
     */
    public function parse()
    {
        $content = $this->read( $this->file_dir . DIRECTORY_SEPARATOR . $this->file );
        if ( $content ) {
            $parser = new Parser( $content );
            $result = $parser->getTable();

            return $result;
        } else {
            return false;
        }
    }

    /**
     * Reads file
     *
     * @param $file string
     *
     * @return string|bool
     */
    private function read( $file )
    {
        if ( file_exists( $file ) ) {
            $fp = @fopen( $file, 'r' ) or die();
            $content = fread( $fp, filesize( $file ) );
            fclose( $fp );

            return $content;
        } else {
            return false;
        }
    }

    /**
     * Get file info based on file name
     *
     * @return array|string
     */
    public function getFileInfo()
    {
        $parts = explode( '.', $this->file );
        if ( $parts[ 1 ] === 'txt' ) {
            $tmp = explode( '-', $parts[ 0 ] );
            if ( count( $tmp ) >= 2 ) {
                $result[ 'class' ]   = $tmp[ 0 ];
                $result[ 'edition' ] = $tmp[ 1 ];

                return $result;
            } else {
                return 'Ime fajla nije ispravno!';
            }
        } else {
            return 'Ime fajla nije ispravno!';
        }
    }
}


