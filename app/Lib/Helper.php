<?php
/**
 * Helper class
 *
 * @author Bosko Stupar <bosko.stupar@gmail.com>
 *
 */

namespace App\Lib;

use DB;
use App\Lib\DamerauLevenshtein as Damerau;

class Helper
{
    protected static $full_match_offset = 5;
    protected static $single_match_offset = 3;

    /**
     * Compare two string for exact match
     *
     * @param $class_text
     * @param $class_heading
     *
     * @return bool|string
     */
    public static function compareStr( $class_heading, $class_text )
    {
        // explode strings to ; to split them
        // and then loop through them
        $value1 = explode( ";", $class_text );
        $value2 = explode( ";", $class_heading );

        //counter
        $matched_num = 0;
        $closest = '';

        // it can happen that we pass strings with similar lengths
        // so it's good to check them before continuing further
        $size_check = self::sizeCheck( $class_heading, $class_text );
        if ( $size_check ) {
            return $size_check;
        }

        // loop through each part of first string
        foreach ( $value1 as $key1 => $part1 ) {
            if ( $part1 !== '' ) {
                // loop through each part of second string
                foreach ( $value2 as $key2 => $part2 ) {
                    if ( $part2 && $part2 !== '' ) {
                        // do matching and trim input values from . , and ; and set them to lower case
                        $damerau = Damerau::distancei( self::prepareStr( $part1 ), self::prepareStr( $part2 ) );
                        // allowed error is 2 characters
                        if ( $damerau < self::$single_match_offset ) {
                            if ( $closest == '' ) {
                                $closest = $part1;
                            } else {
                                $closest = $closest . ';' . $part1;
                            }
                            // remove matched part from array - we don't want duplicates !
                            array_splice( $value2, array_search( $part1, $value2 ), 1 );
                            $matched_num++;
                        }
                    }
                }
            }
        }
        // we need size of matched string and then check with length of exploded class heading
        // if it's the same, it's counted as full match !
        // if $closest is not set, it's no match and finaly if it's set it's incomplete
         $ch_size = count( explode( ';', $class_heading ) );

        if ($matched_num == $ch_size) {
            return true;
        } if ( $closest === '' ) {
            return false;
        } else {
            return $closest;
        }

    }


    public static function colorMatch( $class_heading, $class_text)
    {
        // explode strings to ; to split them
        // and then loop through them
        $value1 = explode( ";", $class_text );
        $value2 = explode( ";", $class_heading );

        $closest = '';

        foreach ( $value1 as $key1 => $part1 ) {
            if ( $part1 !== '' ) {
                // loop through each part of second string
                foreach ( $value2 as $key2 => $part2 ) {
                    if ( $part2 && $part2 !== '' ) {
                        // do matching and trim input values from . , and ; and set them to lower case
                        $damerau = Damerau::distancei( self::prepareStr( $part1 ), self::prepareStr( $part2 ) );
                        // allowed error is 2 characters
                        if ( $damerau < self::$single_match_offset ) {
                            if ( $closest == '' ) {
                                $closest = str_replace_first($part1, '<span style="color:red">'.$part1.'</span>', $class_text);
                            } else {
                                $closest = str_replace_first($part1, '<span style="color:red">'.$part1.'</span>', $closest);
                            }
                            // remove matched part from array - we don't want duplicates !
                            array_splice( $value2, array_search( $part1, $value2 ), 1 );
                        }
                    }
                }
            }
        }

        return $closest;
    }

    function str_replace_first($search, $replace, $subject) {
        $pos = strpos($subject, $search);
        if ($pos !== false) {
            return substr_replace($subject, $replace, $pos, strlen($search));
        }
        return $subject;
    }
    /**
     * a test idea for comparing strings so that we can cross the gap with misplaced ; in string!
     *
     * @param $class_text
     * @param $class_heading
     *
     * @return bool|string
     */
    public static function compareStrStr( $class_heading, $class_text )
    {
        $text_part = '';
        $str_to_match = [ 'namely', 'being', 'including', 'especially', 'in particular', 'mainly' ];

        for ( $i = 0; $i < count( $str_to_match ); $i++ ) {
            $str_part = strstr( $class_text, $str_to_match[ $i ], true );
            if ( $str_part !== false ) {
                $text_part = $str_part;
            }
        }
        // if we found some of $str_to_match in the string, we check for first part and to the getMatch comparison
        // else we do compareStr
        if ( $text_part !== '' ) {
            return self::sizeCheck( $class_heading, $text_part );
        } else {
            return self::compareStr( $class_heading, $class_text );
        }

    }

    /**
     * Check the length of the strings and try to match them
     * if not return false (sizes don't match in defined offset)
     *
     * @param $heading
     * @param $class_text
     *
     * @return bool
     */
    public static function sizeCheck( $heading, $class_text )
    {
        $heading_size = strlen( $heading );
        $class_size = strlen( $class_text );

        switch ( max( $heading_size, $class_size ) ) {
            case $heading_size:
                if ( ( $heading_size - $class_size ) < self::$full_match_offset ) {
                    return self::getMatch( $heading, $class_text );
                }
                break;
            case $class_size:
                if ( ( $class_size - $heading_size ) < self::$full_match_offset ) {
                    return self::getMatch( $heading, $class_text );
                }
                break;
        }

        return false;
    }

    /**
     * Check if the whole string is matched
     *
     * @param $class_text
     * @param $heading
     *
     * @return bool
     */
    public static function checkFullMatch( $heading, $class_text )
    {
        // check if strings are empty
        if ( $class_text == '' or $heading == '' ) {
            return false;
        }
        //get the lenght of thge strings
        $heading_size = strlen( $heading );
        $class_size = strlen( $class_text );

        if ( $class_size != $heading_size ) {
            //we need largest string from those two
            //then check if the difference is less that 5 ($full_match_offset) characters
            //if it is, do the comparing with trimmed strings and return boolean
//            $size_check = self::sizeCheck( $heading, $class_text );
//            if ( $size_check ) {
//                return $size_check;
//            } else if ( !$size_check ) {
                // if difference is more than $full_match_offset characters
                // pass values to other function for comparing and check if the result is
                // string or bool. If it's string than we don't have match !!!
                $compare = self::sizeCheck( $heading, $class_text );
                if ( is_bool( $compare ) ) {
                    return $compare;
                } else {
                    return false;
                }



        } else {
            return self::getMatch( $heading, $class_text );
        }

    }

    /**
     * Remove . , and ; from string so we can easily do the comparison
     *
     * @param $str
     *
     * @return string
     */
    public static function prepareStr( $str )
    {
        $remove = [ '.', ',', ';', '-' ];
        $str = str_replace( $remove, '', $str );

        return strtolower( $str );
    }

    /**
     * Method for matching strings
     *
     * @param $heading
     * @param $matched
     *
     * @return bool
     */
    public static function getMatch( $heading, $matched )
    {
        // check match for trimmed strings
        $damerau = Damerau::distancei( self::prepareStr( $heading ), self::prepareStr( $matched ) );
        if ( $damerau < self::$full_match_offset ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Add color on matched string parts
     *
     * @param $matched
     * @param $heading
     *
     * @return string
     */
    public static function compareStrColored( $matched, $heading )
    {
        $value1 = explode( ";", $matched );
        $value2 = explode( ';', $heading );

        $larger = '';
        $smaller = '';
        if ( count( $value1 ) > count( $value2 ) ) {
            $larger = $matched;
            $smaller = $value2;
        }
        if ( count( $value1 ) < count( $value2 ) ) {
            $larger = $heading;
            $smaller = $value1;
        } else {
            $larger = $matched;
            $smaller = $value2;
        }

        $response = $larger;
        foreach ( $smaller as $val ) {
            if ( $val !== '' ) {
                $rep = '<span style="color:#07ce1a">' . str_replace( '.', '', $val ) . "</span>";
                $response = str_ireplace( str_replace( '.', '', $val ), $rep, $response );
            }
        }

        return self::namelyBeing( $response );

    }

    /**
     * Add span color style to string if it contains namely or being in it
     *
     * @param $str
     *
     * @return string
     */
    public static function namelyBeing( $str )
    {
        $arr = [ 'namely', 'being', 'including', 'especially', 'in particular', 'mainly' ];
        $response = $str;
        foreach ( $arr as $key => $val ) {
            $rep = '<span style="color:#ce061c">' . $val . "</span>";
            $response = str_ireplace( $val, $rep, $response );
        }

        return $response;
    }

    /**
     * Reformat results after json parsing
     *
     * @param $arr
     *
     * @return json
     */
    public static function reformatArray( $arr )
    {
        $result = '';

        foreach ( $arr as $number => $match ) {
            $to_json = '';
            foreach ( $match[ 'niceclasses' ] as $val ) {
                $to_json[ $val ] = $match[ $val ][ 'matched' ];
            }
            $result[ $number ] = json_encode( $to_json );
        }

        return $result;
    }

    /**
     * Get birth date base od JMBG
     *
     * @param $jmbg
     *
     * @return string
     */
    public static function jmbgToDate( $jmbg )
    {
        $parts = str_split( $jmbg );
        $size = count( $parts );

        if ( $size === 13 ) {
            $day = $parts[ 0 ] . $parts[ 1 ];
            $month = $parts[ 2 ] . $parts[ 3 ];
            $year = '';

            if ( $parts[ 4 ] == 9 ) {
                $year = '1' . $parts[ 4 ] . $parts[ 5 ] . $parts[ 6 ];
            }
            if ( $parts[ 4 ] == 0 ) {
                $year = '2' . $parts[ 4 ] . $parts[ 5 ] . $parts[ 6 ];
            }

            return $day . '-' . $month . '-' . $year;
        } else {
            return "JMBG nije validan";
        }
    }

    /**
     * Return person's sex based on JMBG
     *
     * @param $jmbg
     *
     * @return string
     */
    public static function sexByJmbg( $jmbg )
    {
        $parts = str_split( $jmbg );
        $size = count( $parts );

        if ( $size === 13 ) {
            $sex = $parts[ 9 ] . $parts[ 10 ] . $parts[ 11 ];
            if ( $sex < 500 ) {
                return "muski";
            }
            if ( $sex >= 500 ) {
                return "zenski";
            }
        } else {
            return "JMBG nije validan";
        }
    }

    /**
     * Latin to Cyrilic translator
     *
     * @param $str
     *
     * @return string
     */
    public static function toCyrl( $str )
    {
        $letters = [
            "dž" => "џ", "lj" => "љ",
            "nj" => "њ", "a" => "а",
            "b"  => "б", "v" => "в",
            "g"  => "г", "d" => "д",
            "đ"  => "ђ", "e" => "е",
            "ž"  => "ж", "z" => "з",
            "i"  => "и", "j" => "ј",
            "k"  => "к", "l" => "л",
            "m"  => "м", "n" => "н",
            "o"  => "о", "p" => "п",
            "r"  => "р", "s" => "с",
            "t"  => "т", "ć" => "ћ",
            "u"  => "у", "f" => "ф",
            "h"  => "х", "c" => "ц",
            "č"  => "ч", "š" => "ш",
            "DŽ" => "Џ", "NJ" => "Њ",
            "LJ" => "Љ", "A" => "А",
            "B"  => "Б", "V" => "В",
            "G"  => "Г", "D" => "Д",
            "Đ"  => "Ђ", "E" => "Е",
            "Ž"  => "Ж", "Z" => "З",
            "I"  => "И", "J" => "Ј",
            "K"  => "К", "L" => "Л",
            "M"  => "М", "N" => "Н",
            "O"  => "О", "P" => "П",
            "R"  => "Р", "S" => "С",
            "T"  => "Т", "Ć" => "Ћ",
            "U"  => "У", "F" => "Ф",
            "H"  => "Х", "C" => "Ц",
            "Č"  => "Ч", "Š" => "Ш" ];

        $result = $str;
        foreach ( $letters as $lat => $cyrl ) {
            $result = str_replace( $lat, $cyrl, $result );
        }

        return $result;
    }

    public static function tmName( $json )
    {
        $json = json_decode($json->json);
        return $$json->entity->name;
    }
}
